package study.wechat.applet.dao.userwechat;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface UserWechatDao extends BaseMapper<UserWechatEO> {

    default boolean isPresent(String openId) {
        UserWechatEO uwEO = getByOpenId(openId);
        boolean present = false;
        if (uwEO != null) {
            present = true;
        }
        return present;
    }

    default UserWechatEO getByOpenId(String openId) {
        QueryWrapper<UserWechatEO> wrapper = new QueryWrapper();
        //条件
        wrapper.eq("uw_open_id", openId)
                .last("limit 1");
        UserWechatEO uwEO = selectOne(wrapper);
        return uwEO;
    }
}
