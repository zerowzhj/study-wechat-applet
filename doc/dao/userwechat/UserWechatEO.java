package study.wechat.applet.dao.userwechat;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("UserWechatEO")
@TableName("user_wechat")
public class UserWechatEO {
    @TableId(value = "uw_id")
    private Long uwId;
    private String uwOpenId;
    private String uwUnionId;
}
