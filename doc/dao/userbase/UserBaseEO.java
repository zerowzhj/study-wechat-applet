package study.wechat.applet.dao.userbase;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("UserBaseEO")
@TableName("user_baseUserBaseEO")
public class UserBaseEO {
    private Long ubId;
}
