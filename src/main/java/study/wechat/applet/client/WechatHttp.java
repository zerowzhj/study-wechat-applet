package study.wechat.applet.client;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import study.eggs.utils.JsonUtils;

import javax.annotation.PostConstruct;
import java.util.Map;

@Slf4j
@Component
public class WechatHttp {

    private static String HOST = "https://api.weixin.qq.com";

    private static RestTemplate CLIENT;

    @Autowired
    private RestTemplate restTemplate;

    @PostConstruct
    public void init() {
        CLIENT = restTemplate;
    }

    public static String get(String path, Map<String, Object> param) {
        log.info("[{}] i: {}", path, JsonUtils.toJson(param));
        // url
        String query = MapUtil.join(param, "&", "=");
        String url = StrUtil.builder(HOST, path, "?", query).toString();
        // request
        String body;
        try {
            body = CLIENT.getForObject(url, String.class);
        } catch (Exception ex) {
            throw new RuntimeException(StrUtil.format("请求 wechat [{}]错误: ", path), ex);
        }
        log.info("[{}] o: {}", path, body);
        return body;
    }
}
