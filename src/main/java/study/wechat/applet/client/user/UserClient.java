package study.wechat.applet.client.user;

import cn.hutool.json.JSONUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import study.wechat.applet.client.WechatHttp;
import study.wechat.applet.client.token.TokenClient;

import java.util.Map;

@Slf4j
@Component
public class UserClient {

    private static final String PATH = "/cgi-bin/user/info";

    @Autowired
    private TokenClient tokenClient;

    public UserInfo getUserInfo(String openId) {
        //
        Map<String, Object> param = Maps.newHashMap();
        param.put("access_token", tokenClient.getAccessToken());
        param.put("openid", openId);
        param.put("lang", "zh_CN");
        //
        String body = WechatHttp.get(PATH, param);
        UserInfo info = JSONUtil.toBean(body, UserInfo.class);
        return info;
    }
}
