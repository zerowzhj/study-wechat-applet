package study.wechat.applet.client;

import lombok.Data;

import java.io.Serializable;

@Data
public class WechatBody implements Serializable {
    protected Integer errcode;
    protected String errmsg;
}
