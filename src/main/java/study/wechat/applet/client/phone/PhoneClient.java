package study.wechat.applet.client.phone;

import cn.hutool.json.JSONUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import study.wechat.applet.client.WechatHttp;
import study.wechat.applet.client.token.TokenClient;

import java.util.Map;

/**
 * 获取手机号
 * https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/user-info/phone-number/getPhoneNumber.html
 */
@Slf4j
@Component
public class PhoneClient {

    private static final String PATH = "/wxa/business/getuserphonenumber";

    @Autowired
    private TokenClient tokenClient;

    public PhoneInfo getPhone(String code) {
        Map<String, Object> param = Maps.newHashMap();
        param.put("access_token", tokenClient.getAccessToken());
        param.put("code", code);
        //
        String body = WechatHttp.get(PATH, param);
        PhoneInfo info = JSONUtil.toBean(body, PhoneInfo.class);
        return info;
    }
}
