package study.wechat.applet.client.phone;

import lombok.Data;
import study.wechat.applet.client.WechatBody;

import java.io.Serializable;

@Data
public class PhoneInfo extends WechatBody {

    private String phoneNumber;
    private String purePhoneNumber;
    private String countryCode;
    private Watermark watermark;

    @Data
    public static class Watermark implements Serializable {
        private Long timestamp;
        private String appid;
    }
}
