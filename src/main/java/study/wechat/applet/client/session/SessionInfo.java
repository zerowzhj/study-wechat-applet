package study.wechat.applet.client.session;

import lombok.Data;
import study.wechat.applet.client.WechatBody;

@Data
public class SessionInfo extends WechatBody {
    private String openid;
    private String session_key;
    private String unionid;
}
