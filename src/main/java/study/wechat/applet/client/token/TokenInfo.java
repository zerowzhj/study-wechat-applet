package study.wechat.applet.client.token;

import lombok.Data;
import study.wechat.applet.client.WechatBody;

@Data
public class TokenInfo extends WechatBody {

    private String access_token;
    private Integer expires_in;
}
