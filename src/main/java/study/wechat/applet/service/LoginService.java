package study.wechat.applet.service;

import cn.hutool.core.util.IdUtil;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import study.eggs.result.Result;
import study.eggs.result.Results;
import study.wechat.applet.client.session.SessionClient;
import study.wechat.applet.client.session.SessionInfo;

import java.util.Map;

@Service
public class LoginService {

    @Autowired
    private SessionClient sessionClient;

    public Result login(String code) {
        //step-1: 获取 session 信息
        SessionInfo session = sessionClient.getSessionInfo(code);
        String openId = session.getOpenid();
        String sessionKey = session.getSession_key();
        String unionId = session.getUnionid();
        //step-2: 自定义登录状态与 session 关联
        //userClient.getUserInfo(openId);
//        boolean isPresent = userWechatDao.isPresent(openId);
//        if (!isPresent) {
//            UserWechatEO uwEO = new UserWechatEO();
//            uwEO.setUwOpenId(openId);
//            uwEO.setUwUnionId(unionId);
//            userWechatDao.insert(uwEO);
//        }
        //step-3: 返回自定义登录状态
        String token = IdUtil.fastSimpleUUID();
        Map<String, Object> data = Maps.newHashMap();
        data.put("token", token);
        return Results.ok(data);
    }
}
