package study.wechat.applet.support.session;

import com.alibaba.ttl.TransmittableThreadLocal;

public class UserInfoContext {

    private static final TransmittableThreadLocal<UserInfo> LOCAL = new TransmittableThreadLocal<>();

    public static UserInfo get() {
        return LOCAL.get();
    }

    public static void set(UserInfo userInfo) {
        LOCAL.set(userInfo);
    }

    public static void remove() {
        LOCAL.remove();
    }
}
