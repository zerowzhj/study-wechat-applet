package study.wechat.applet.support.session;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserInfo implements Serializable {
    private final String timestamp;
    private Long ubId;
    private String ubName;
    private String ubEmail;
}
