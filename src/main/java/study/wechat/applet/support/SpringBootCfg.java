package study.wechat.applet.support;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "study.wechat.applet")
public class SpringBootCfg {

}
