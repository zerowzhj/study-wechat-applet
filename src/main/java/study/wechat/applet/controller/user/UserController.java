package study.wechat.applet.controller.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import study.eggs.result.Result;
import study.eggs.result.Results;

@Slf4j
@RestController
public class UserController {

    @RequestMapping("/getUserInfo")
    public Result getUserInfo() {
        log.info("ssssssssss");
        return Results.ok();
    }
}
