package study.wechat.applet.controller.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import study.eggs.result.Result;
import study.wechat.applet.service.LoginService;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public Result login(@RequestBody LoginRequest request) {
        String jsCode = request.getJsCode();
        if (1 == 1) {

        }
        return loginService.login(jsCode);
    }
}
