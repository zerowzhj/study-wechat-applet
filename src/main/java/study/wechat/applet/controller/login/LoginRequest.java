package study.wechat.applet.controller.login;

import lombok.Data;

@Data
public class LoginRequest {
    private String jsCode;
}
