package study.wechat.applet;

import org.springframework.boot.SpringApplication;
import study.wechat.applet.support.SpringBootCfg;

public class Startup {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootCfg.class, args);
    }
}
