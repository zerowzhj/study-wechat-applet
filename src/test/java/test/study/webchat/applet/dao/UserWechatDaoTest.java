//package test.study.webchat.applet.dao;
//
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//import study.wechat.applet.dao.userwechat.UserWechatDao;
//import study.wechat.applet.dao.userwechat.UserWechatEO;
//import study.wechat.applet.support.SpringBootCfg;
//
//@Slf4j
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {SpringBootCfg.class})
//public class UserWechatDaoTest {
//
//    @Autowired
//    private UserWechatDao userWechatDao;
//
//    @Test
//    public void test() {
//        UserWechatEO uwEO = new UserWechatEO();
//        uwEO.setUwOpenId("aaaaaaaaaa");
//        userWechatDao.insert(uwEO);
//    }
//}
