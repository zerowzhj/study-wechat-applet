package test.study.webchat.applet.client;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import study.wechat.applet.client.token.TokenClient;
import study.wechat.applet.client.token.TokenInfo;
import study.wechat.applet.support.SpringBootCfg;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootCfg.class})
public class TokenClientTest {

    @Autowired
    private TokenClient tokenClient;

    @Test
    public void test(){
        TokenInfo info = tokenClient.getAccessToken();
        //log.info("{}", info);
    }
}
