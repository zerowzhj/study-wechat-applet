package test.study.webchat.applet.client;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import study.wechat.applet.client.session.SessionClient;
import study.wechat.applet.client.session.SessionInfo;
import study.wechat.applet.support.SpringBootCfg;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootCfg.class})
public class LoginClientTest {

    @Autowired
    private SessionClient sessionClient;

    @Test
    public void test(){
        String code = "033fgJ000woJCM1PMI200A5XyK0fgJ0P";
        SessionInfo info = sessionClient.getSessionInfo(code);
        log.info("{}", info);
    }
}
