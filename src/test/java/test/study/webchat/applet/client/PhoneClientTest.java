package test.study.webchat.applet.client;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import study.wechat.applet.client.phone.PhoneClient;
import study.wechat.applet.client.phone.PhoneInfo;
import study.wechat.applet.support.SpringBootCfg;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootCfg.class})
public class PhoneClientTest {

    @Autowired
    private PhoneClient phoneClient;

    @Test
    public void getPhone_test() {
        String code = "oFEnU4nJ1xeYcIHk3SEvcPG929dI";
        PhoneInfo phoneInfo = phoneClient.getPhone(code);
    }
}
