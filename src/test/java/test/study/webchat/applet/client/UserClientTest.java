package test.study.webchat.applet.client;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import study.wechat.applet.client.user.UserClient;
import study.wechat.applet.client.user.UserInfo;
import study.wechat.applet.support.SpringBootCfg;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootCfg.class})
public class UserClientTest {

    @Autowired
    private UserClient userInfoClient;

    @Test
    public void getUserInfo_test() {
        String openId = "oFEnU4nJ1xeYcIHk3SEvcPG929dI";
        UserInfo userInfo = userInfoClient.getUserInfo(openId);
    }
}
